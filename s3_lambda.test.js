const s3_lambda = require("./s3_lambda");
const AWS = require("aws-sdk");
// const { describe } = require("yargs");
// const { beforeAll, test } = require("@jest/globals");

//AWS.config.update({region: 'us-east-1'});

describe("Lambda function exists", () => {
    beforeAll(async() => {
        var creds = new AWS.SharedIniFileCredentials({profile: 'default'});
        AWS.config.credentials = creds;
    });

    test("Method exists", () => {
        expect(typeof s3_lambda.list_buckets).toBe("function");
    });

    test("Returns 4 buckets", async () => {
        let event = {};
        let json = await s3_lambda.list_buckets(event);
        //let obj = JSON.parse(json);
        expect(json.Buckets.length).toEqual(4);
    });
});