const AWS = require('aws-sdk');

exports.list_buckets = async (event) => {
    var s3 = new AWS.S3();
    var response = s3.listBuckets().promise();
    console.log(response);
    return response;
}